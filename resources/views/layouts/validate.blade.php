@if (session()->has('success'))
    <div class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-9 text-white">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
             fill="none" stroke="currentColor" stroke-width="1.5" stroke-linecap="round"
             stroke-linejoin="round" class="feather feather-alert-circle w-6 h-6 mr-2">
            <circle cx="12" cy="12" r="10"></circle>
            <line x1="12" y1="8" x2="12" y2="12"></line>
            <line x1="12" y1="16" x2="12.01" y2="16"></line>
        </svg>
        {{session()->get('success')}}
    </div>

@endif

@if (session()->has('error'))
    <div
        class="rounded-md flex items-center px-5 py-4 mb-2 bg-theme-6 text-white">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
             viewBox="0 0 24 24" fill="none" stroke="currentColor"
             stroke-width="1.5" stroke-linecap="round"
             stroke-linejoin="round"
             class="feather feather-alert-circle w-6 h-6 mr-2">
            <circle cx="12" cy="12" r="10"></circle>
            <line x1="12" y1="8" x2="12" y2="12"></line>
            <line x1="12" y1="16" x2="12.01" y2="16"></line>
        </svg>
        {{session()->get('error')}}
    </div>
@endif
@if($errors->any())
    {!! implode('', $errors->all('<div>:message</div>')) !!}
@endif
