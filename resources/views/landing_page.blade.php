<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>صفحه ی اصلی</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Vendor CSS Files -->
    <link href="/css/Landing/aos.css" rel="stylesheet">
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/Landing/glightbox.min.css" rel="stylesheet">

    <link href="/fonts/boxicons.css" rel="stylesheet">
    <link href="/fonts/font.css" rel="stylesheet">

    <!-- Template Main CSS File -->
    <link href="/css/Landing/landing.css" rel="stylesheet">

    <!-- =======================================================
    * Template Name: Bootslander
    * Updated: Jan 29 2024 with Bootstrap v5.3.2
    * Template URL: https://bootstrapmade.com/bootslander-free-bootstrap-landing-page-template/
    * Author: BootstrapMade.com
    * License: https://bootstrapmade.com/license/
    ======================================================== -->
</head>

<body>

<!-- ======= Header ======= -->
<header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container d-flex align-items-center justify-content-between">

        <div class="logo">
            <h1><a href="/index.html"><span>دریانو</span></a></h1>
            <!-- Uncomment below if you prefer to use an image logo -->
            <!-- <a href="/index.html"><img src="/images/logo.png" alt="" class="img-fluid"></a>-->
        </div>

        <nav id="navbar" class="navbar">
            <ul>
                <li><a class="nav-link scrollto" href="#contact">تماس با ما</a></li>
                <li><a class="nav-link scrollto" href="#team">تیم دریانو</a></li>
                <li><a class="nav-link scrollto" href="#gallery">پروژه ها</a></li>
                <li><a class="nav-link scrollto" href="#features">مشاور</a></li>
                <li><a class="nav-link scrollto" href="#about">سرمایه گذار</a></li>
                <li><a class="nav-link scrollto active" href="#hero">صفحه ی اصلی</a></li>
            </ul>
            <i class="bi bi-list mobile-nav-toggle"></i>
        </nav><!-- .navbar -->

    </div>
</header><!-- End Header -->

<!-- ======= Hero Section ======= -->
<section id="hero">

    <div class="container">
        <div class="row justify-content-between">
            <div class="col-lg-4 order-2 order-lg-1 hero-img" data-aos="zoom-out" data-aos-delay="300">
                <img src="//images/hero-img.png" class="img-fluid animated" alt="">
            </div>
            <div class="col-lg-7 pt-5 pt-lg-0 order-1 order-lg-2 d-flex align-items-center" style="text-align: right;">
                <div data-aos="zoom-out">
                    <h1>با دریانو یک <span> سرمایه گذاری پر سود </span> را تجربه کنید</h1>
                    <h2>با داشبورد مشاورین خیالتون راحته !</h2>
                    <div class="text-center text-lg-start">
                        <a href="#about" class="btn-get-started scrollto">شروع سرمایه گذاری</a>
                    </div>
                </div>
            </div>

        </div>
    </div>

    <svg class="hero-waves" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
         viewBox="0 24 150 28 " preserveAspectRatio="none">
        <defs>
            <path id="wave-path" d="M-160 44c30 0 58-18 88-18s 58 18 88 18 58-18 88-18 58 18 88 18 v44h-352z">
        </defs>
        <g class="wave1">
            <use xlink:href="#wave-path" x="50" y="3" fill="rgba(255,255,255, .1)">
        </g>
        <g class="wave2">
            <use xlink:href="#wave-path" x="50" y="0" fill="rgba(255,255,255, .2)">
        </g>
        <g class="wave3">
            <use xlink:href="#wave-path" x="50" y="9" fill="#fff">
        </g>
    </svg>

</section><!-- End Hero -->

<main id="main">

    <!-- ======= About Section ======= -->
    <section id="about" class="details">
        <div class="container">
            <div class="section-title aos-init aos-animate" data-aos="fade-up" style="direction: rtl;">
                <h2>سرمایه گذار</h2>
                <p>سرمایه گذاری در املاک</p>
            </div>
            @foreach($sliders as $slider)

                @if($slider->available == 'show')
                    <div class="row content">
                        <div class="col-md-4 order-1 order-md-2 aos-init aos-animate" data-aos="fade-left">
                            <a href="/images/gharardad1.jpg" class="gallery-lightbox">
                                <img src="//images/gharardad1.jpg" alt="" class="img-fluid">
                            </a>
                        </div>
                        <div class="col-md-8 pt-5 order-2 order-md-1 aos-init aos-animate" data-aos="fade-up"
                             style="text-align: right;">
                            <h3> {{$slider->title}}</h3>
                            <p class="fst-italic">
                                {{$slider->description}}
                            </p>

                        </div>
                    </div>
                @endif
            @endforeach
            <div class="progress" style="height: 35px;">
                <div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar"
                     style="width: 75%" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100">
                    <span> درصد سرمایه گذاری شده </span>
                    <p> 75% </p>
                </div>
            </div>

            <div class="text-left">
                <button class="btn btn-success" type="submit">شروع سرمایه گذاری</button>
            </div>
        </div>
    </section>

    <!-- ======= Counts Section ======= -->
    <section id="counts" class="counts">
        <div class="container">

            <div class="row" data-aos="fade-up">

                <div class="col-lg-3 col-md-6">
                    <div class="count-box">
                        <i class="bi bi-emoji-smile"></i>
                        <span data-purecounter-start="0" data-purecounter-end="232" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>سرمایه گذاری های انجام شده</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                    <div class="count-box">
                        <i class="bi bi-journal-richtext"></i>
                        <span data-purecounter-start="0" data-purecounter-end="521" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>پروژه ها</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="count-box">
                        <i class="bi bi-headset"></i>
                        <span data-purecounter-start="0" data-purecounter-end="1463" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>مشاور ها</p>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="count-box">
                        <i class="bi bi-people"></i>
                        <span data-purecounter-start="0" data-purecounter-end="15" data-purecounter-duration="1"
                              class="purecounter"></span>
                        <p>سرمایه گذار ها</p>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Counts Section -->

    <section id="features" class="features">
        <div class="container">
            <div class="section-title aos-init aos-animate" data-aos="fade-up" style="direction: rtl;">
                <h2>مشاور</h2>
                <p>مشاوره</p>
            </div>
            <div class="row content">
                <div class="col-md-4 aos-init aos-animate" data-aos="fade-right">
                    <a href="/images/gharardad2.jpg" class="gallery-lightbox">
                        <img src="//images/gharardad2.jpg" alt="" class="img-fluid">
                    </a>
                </div>
                <div class="col-md-8 pt-4 aos-init aos-animate" data-aos="fade-up">
                    <p class="fst-italic">
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ، و با استفاده از طراحان گرافیک است،
                        چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است، و برای شرایط فعلی تکنولوژی
                        مورد نیاز، و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد، کتابهای زیادی در شصت و سه
                        درصد گذشته حال و آینده، شناخت فراوان جامعه و متخصصان را می طلبد، تا با نرم افزارها شناخت بیشتری
                        را برای طراحان رایانه ای علی الخصوص طراحان خلاقی، و فرهنگ پیشرو در زبان فارسی ایجاد کرد، در این
                        صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها، و شرایط سخت تایپ به پایان رسد و
                        زمان مورد نیاز شامل حروفچینی دستاوردهای اصلی، و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی
                        اساسا مورد استفاده قرار گیرد.
                    </p>
                </div>
            </div>

            <div style="text-align: end;">
                <button class="btn btn-primary" type="submit">شروع همکاری</button>
            </div>

        </div>
    </section>


    <!-- ======= Gallery Section ======= -->
    <section id="gallery" class="gallery">
        <div class="container">

            <div style="direction: rtl;" class="section-title" data-aos="fade-up">
                <h2>پروژه</h2>
                <p>پروژه ها</p>
            </div>

            <div class="row g-0" data-aos="fade-left">

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="100">
                        <a href="/images/gallery/gallery-1.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-1.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="150">
                        <a href="/images/gallery/gallery-2.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-2.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="200">
                        <a href="/images/gallery/gallery-3.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-3.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="250">
                        <a href="/images/gallery/gallery-4.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-4.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="300">
                        <a href="/images/gallery/gallery-5.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-5.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="350">
                        <a href="/images/gallery/gallery-6.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-6.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="400">
                        <a href="/images/gallery/gallery-7.jpg" class="gallery-lightbox">
                            <img src="//images/gallery/gallery-7.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

                <div class="col-lg-3 col-md-4">
                    <div class="gallery-item" data-aos="zoom-in" data-aos-delay="450">
                        <a href="/images/gallery/gallery-8.jpg" class="gallery-lightbox">
                            <img src="/images/gallery/gallery-8.jpg" alt="" class="img-fluid">
                        </a>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Gallery Section -->

    <!-- ======= Team Section ======= -->
    <section id="team" class="team">
        <div class="container">

            <div style="direction: rtl;" class="section-title" data-aos="fade-up">
                <h2>اعضا</h2>
                <p>تیم دریانو</p>
            </div>

            <div class="row" data-aos="fade-left">

                <div class="col-lg-3 col-md-6">
                    <div class="member" data-aos="zoom-in" data-aos-delay="100">
                        <div class="pic"><img src="/images/team/team-1.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info">
                            <h4>وحید بهمنی</h4>
                            <span>هیلر</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-md-0">
                    <div class="member" data-aos="zoom-in" data-aos-delay="200">
                        <div class="pic"><img src="/images/team/team-2.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info">
                            <h4>منشی</h4>
                            <span>تانک</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="member" data-aos="zoom-in" data-aos-delay="300">
                        <div class="pic"><img src="/images/team/team-3.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info">
                            <h4>وکیل</h4>
                            <span>فایتر</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 mt-5 mt-lg-0">
                    <div class="member" data-aos="zoom-in" data-aos-delay="400">
                        <div class="pic"><img src="/images/team/team-4.jpg" class="img-fluid" alt=""></div>
                        <div class="member-info">
                            <h4>زن بهمنی</h4>
                            <span>اینتل</span>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </section><!-- End Team Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contact" class="contact">
        <div class="container">

            <div style="direction: rtl;" class="section-title" data-aos="fade-up">
                <h2>اطلاعات تماس</h2>
                <p>تماس با ما</p>
            </div>

            <div class="row">

                <div class="col-lg-12" data-aos="fade-right" data-aos-delay="100">
                    <div class="info">
                        <div class="address">
                            <i class="bi bi-geo-alt"></i>
                            <h4>آدرس : </h4>
                            <p>پرند</p>
                        </div>

                        <div class="email">
                            <i class="bi bi-envelope"></i>
                            <h4>ایمیل :</h4>
                            <p>daryano@mail.com</p>
                        </div>

                        <div class="phone">
                            <i class="bi bi-phone"></i>
                            <h4>تماس :</h4>
                            <p>021-123456789</p>
                        </div>

                    </div>

                </div>

            </div>

        </div>
    </section><!-- End Contact Section -->

</main><!-- End #main -->

<!-- ======= Footer ======= -->
<footer id="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-lg-2 col-md-6 footer-links">
                    <h4>لینک های مفید</h4>
                    <ul>
                        <li><i class="bx bx-chevron-right"></i> <a href="#features">مشاور</a></li>
                        <li><i class="bx bx-chevron-right"></i> <a href="#about">سرمایه گذار</a></li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="footer-info">
                        <h3>دریانو</h3>
                        <p class="pb-3"><em>آدرس : پرند ...</em></p>
                        <p>
                            <strong>شماره تماس:</strong> 021-123456789 <br>
                            <strong>ایمیل:</strong> mail@mail.com <br>
                        </p>
                        <div class="social-links mt-3">
                            <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
                            <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
                            <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
                            <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
                            <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 col-md-6">
                    <div class="footer-info">
                        <h3>اینماد</h3>
                    </div>
                </div>


            </div>
        </div>
    </div>

    <div class="container">
        <div class="copyright">
            تمامی حقوق محفوظ است ©
        </div>
        <div class="credits">
            طراحی شده توسط تیم <a href="https://www.backtick.ir">بک تیک</a>
        </div>
    </div>
</footer><!-- End Footer -->

<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class='bx bx-up-arrow-alt'></i></a>

<script src="/js/Landing/aos.js"></script>
<script src="/js/bootstrap.bundle.min.js"></script>
<script src="/js/Landing/glightbox.min.js"></script>
<script src="/js/Landing/swiper-bundle.min.js"></script>


<!-- Template Main JS File -->
<script src="/js/Landing/landing.js"></script>

</body>

</html>
