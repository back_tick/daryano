@extends('admin.layouts.main')
@section('title')
    لندینگ پیج
@endsection


@section('content')
    <div style="text-align: end" class="container-xxl flex-grow-1 container-p-y">
        <div class="row">

            <!-- Basic -->
            <div class="card">
                <div class="alert alert-dismissible alert-dark">
                    <h5 class="card-header">لیست المنت های لندیگ پیج</h5>
                </div>

                <div class="alert alert-dismissible">
                    <button type="button" class="btn rounded-pill btn-primary">
                        <i class='bx bx-plus'></i> افزودن
                    </button>
                </div>

                <div style="padding-bottom: 100px;" class="table-responsive text-nowrap">
                    <table class="table table-hover table-striped">
                        <thead>
                        <tr>
                            <th>عملیات</th>
                            <th>نمایش داده میشود ؟</th>
                            <th>فایل</th>
                            <th>توضیحات</th>
                            <th>عنوان</th>
                        </tr>
                        </thead>
                        <tbody class="table-border-bottom-0">
                        @foreach($sliders as  $slider)
                            <tr>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                                data-bs-toggle="dropdown">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="javascript:void(0);"><i
                                                    class="bx bx-edit-alt me-1"></i> ویرایش</a>
                                            <a class="dropdown-item" href="{{route('admin.landing_slider.delete',$slider)}}"><i
                                                    class="bx bx-trash me-1"></i> حذف</a>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    <i class="fab fa-angular fa-lg text-danger me-3"></i>
                                        <strong>@if($slider->available == "show")نمایش داره می شود @else پنهان می باشد @endif </strong> </td>
                                <td>
                                    @if(isset($slider->file_path))فایل دارد @else فایل ندارد @endif
                                </td>
                                <td>
                                <td>{{$slider->description}}</td>
                                <td><span class="badge bg-label-primary me-1">{{$slider->title}}</span></td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                    @isset($sliders)
                        <nav aria-label="Page navigation">
                            @include('layouts.pagination', ['paginator' => $sliders])

                        </nav>
                    @endisset
                </div>
            </div>
        </div>

    </div>
@endsection
