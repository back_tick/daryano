@extends('admin.layouts.main')

@section('content')
    <div style="text-align: end" class="container-xxl flex-grow-1 container-p-y">
        <div class="row">

            @include('layouts.validate')
            <!-- Basic -->
            <div class="col-xl">
                <div class="card mb-4">
                    <div class="alert alert-dismissible alert-secondary">
                        <button class="btn btn-primary mb-3" type="button" data-area="1" data-square="1">
                            افزودن به صفحه
                        </button>
                    </div>
                    <div class="card-body">
                        <form action="{{route('admin.landing_slider.store')}}" method="post"  enctype="multipart/form-data">
                            <div class="mb-3">
                                <label class="form-label" for="basic-icon-default-fullname">عنوان</label>
                                <div class="input-group input-group-merge">
                                        <span id="basic-icon-default-fullname2" class="input-group-text">
                                            <i class="bx bx-user"></i>
                                        </span>
                                    <input id="squarename_fill" class="form-control" placeholder="عنوان را وارد کنید"
                                           aria-describedby="basic-icon-default-fullname2" type="text" data-val="true"
                                           data-val-maxlength="The field نام کاربری must be a string or array type with a maximum length of '20'."
                                           data-val-maxlength-max="20" data-val-regex="کاراکتر های مجاز : 9-0 , a-z"
                                           data-val-regex-pattern="^[A-Za-z0-9]+$"
                                           data-val-required="لطفا عنوان را وارد کنید" maxlength="20"
                                           name="title" value="">
                                </div>
                                <span class="text-danger field-validation-valid" data-valmsg-for="Client.Username"
                                      data-valmsg-replace="true"></span>
                            </div>

                            <div class="mb-3">
                                <label class="form-label" for="basic-icon-default-fullname">نوع</label>
                                <div class="input-group input-group-merge">
                                        <span id="basic-icon-default-fullname2" class="input-group-text">
                                            <i class="bx bx-wrench"></i>
                                        </span>
                                    <select class="form-select" data-val="true"
                                            data-val-maxlength="The field سرویس must be a string or array type with a maximum length of '20'."
                                            data-val-maxlength-max="20" data-val-required="نوع را انتخاب کنید"
                                            id="Client_Service" name="type">
                                        <option value="adviser">مشاور</option>
                                        <option value="investor">سرمایه گذار</option>

                                    </select>
                                </div>
                                <span class="text-danger field-validation-valid" data-valmsg-for="Client.Service"
                                      data-valmsg-replace="true"></span>
                            </div>
                            <div class="mb-3">
                                <label for="formFile" class="form-label">فایل خود را انتخاب کنید</label>
                                <input class="form-control" type="file" id="formFile" name="file">
                                <p> jpg- pdf </p>
                            </div>
{{--                            <div style="direction: rtl;" class="form-check mt-3">--}}
{{--                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1"--}}
{{--                                       name="available">--}}
{{--                                <label class="form-check-label" for="defaultCheck1"> فعال باشد </label>--}}
{{--                            </div>--}}
                            <div class="mb-3">
                                <label class="form-label" for="basic-icon-default-message">توضیحات</label>
                                <div class="input-group input-group-merge">
                                        <span id="basic-icon-default-message2" class="input-group-text">
                                            <i class="bx bx-comment"></i>
                                        </span>
                                    <textarea class="form-control" placeholder="توضیحات را وارد کنید"
                                              aria-describedby="basic-icon-default-message2" data-val="true"
                                              data-val-maxlength="The field توضیحات must be a string or array type with a maximum length of '100'."
                                              data-val-maxlength-max="100" data-val-required="لطفا توضیحات را وارد کنید"
                                              id="Client_Description" maxlength="100"
                                              name="description"></textarea>
                                </div>
                                <span class="text-danger field-validation-valid" data-valmsg-for="Client.Description"
                                      data-valmsg-replace="true"></span>
                            </div>
                            <input type="submit" value="ذخیره" class="btn btn-primary">
                           @csrf
                        </form>
                    </div>
                </div>
            </div>


@endsection
