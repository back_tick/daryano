<ul class="menu-inner py-1">
    <!-- Dashboard -->
    <li class="menu-item active">
        <a href="{{route('admin.dashboard')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-home-circle"></i>
            <div data-i18n="Analytics">داشبورد</div>
        </a>
    </li>

    <li class="menu-header small text-uppercase">
        <span class="menu-header-text">سرمایه گذار</span>
    </li>
    <li class="menu-item">
        <a href="AdminDashbord.html" class="menu-link">
            <i class="menu-icon tf-icons bx bx-detail"></i>
            <div data-i18n="Analytics">لیست سرمایه گذار ها</div>
        </a>
    </li>
    <li class="menu-item">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="menu-icon tf-icons bx bx-dock-top"></i>
            <div data-i18n="Account Settings">منو باز شونده</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item">
                <a href="pages-account-settings-account.html" class="menu-link">
                    <div data-i18n="Account">زیرمنو 1</div>
                </a>
            </li>
            <li class="menu-item">
                <a href="pages-account-settings-notifications.html" class="menu-link">
                    <div data-i18n="Notifications">زیر منو 2</div>
                </a>
            </li>
        </ul>
    </li>

    <li class="menu-header small text-uppercase">
        <span class="menu-header-text">مشاور</span>
    </li>
    <li class="menu-item">
        <a href="AdminDashbord.html" class="menu-link">
            <i class="menu-icon tf-icons bx bx-detail"></i>
            <div data-i18n="Analytics">لیست مشاورین</div>
        </a>
    </li>
    <li class="menu-item">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="menu-icon tf-icons bx bx-dock-top"></i>
            <div data-i18n="Account Settings">منو باز شونده</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item">
                <a href="pages-account-settings-account.html" class="menu-link">
                    <div data-i18n="Account">زیرمنو 1</div>
                </a>
            </li>
            <li class="menu-item">
                <a href="pages-account-settings-notifications.html" class="menu-link">
                    <div data-i18n="Notifications">زیر منو 2</div>
                </a>
            </li>
        </ul>
    </li>

    <li class="menu-header small text-uppercase">
        <span class="menu-header-text"></span>
    </li>

    <li class="menu-item">
        <a href="AdminDashbord.html" class="menu-link">
            <i class="menu-icon tf-icons bx bx-detail"></i>
            <div data-i18n="Analytics">ورود و خروج</div>
        </a>
    </li>
    <li class="menu-item">
        <a href="AdminDashbord.html" class="menu-link">
            <i class="menu-icon tf-icons bx bx-detail"></i>
            <div data-i18n="Analytics">فایل ها</div>
        </a>
    </li>
    <li class="menu-item">
        <a href="{{route('admin.landing_slider.index')}}" class="menu-link">
            <i class="menu-icon tf-icons bx bx-detail"></i>
            <div data-i18n="Analytics">لندینگ پیج</div>
        </a>
    </li>
    <li class="menu-item">
        <a href="javascript:void(0);" class="menu-link menu-toggle">
            <i class="menu-icon tf-icons bx bx-dock-top"></i>
            <div data-i18n="Account Settings">بخش های لندینگ پیج</div>
        </a>
        <ul class="menu-sub">
            <li class="menu-item">
                <a href="{{route('lading_page.setting.create')}}" class="menu-link">
                    <div data-i18n="Account">افزودن اسلایدر</div>
                </a>
            </li>
            <li class="menu-item">
                <a href="pages-account-settings-notifications.html" class="menu-link">
                    <div data-i18n="Notifications">زیر منو 2</div>
                </a>
            </li>
        </ul>
    </li>
</ul>
