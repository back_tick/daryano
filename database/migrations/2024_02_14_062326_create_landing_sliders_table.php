<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('landing_sliders', function (Blueprint $table) {
            $table->id();
            $table->string('file_path')->nullable();
            $table->enum('available',['show','hidden'])->nullable();
            $table->enum('type',['investor','adviser'])->nullable();
            $table->string('title')->nullable();
            $table->text('description')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('landing_sliders');
    }
};
