<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\LandingSlider>
 */
class LandingSliderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    protected $model = \App\Models\LandingSlider::class;
    public function definition()
    {


        return [
            'file_path' => null, // Replace with appropriate image generation/storage logic
            'available' => fake()->randomElement(['show', 'hidden']),
            'type' => fake()->randomElement(['investor', 'adviser']),
            'title' => fake()->sentence(3),
            'description' => fake()->paragraph(2),
            'created_at' => fake()->dateTimeThisMonth,
            'updated_at' => fake()->dateTimeThisMonth,
        ];
    }
//    public function ofType(string $type): LandingSliderFactory
//    {
//        return $this->state(function (Factory $factory) use ($type) {
//            return [
//                'type' => $type,
//            ];
//        });
//    }
//
//    public function withAvailability(string $availability): LandingSliderFactory
//    {
//        return $this->state(function (Factory $factory) use ($availability) {
//            return [
//                'available' => $availability,
//            ];
//        });
//    }

}
