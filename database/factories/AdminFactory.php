<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Admin>
 */
class AdminFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $faker = \Faker\Factory::create();

        return [

            'email' => $faker->unique()->safeEmail,
            'password' => bcrypt('secret'), // Replace with stronger random password generation
            // Add other admin-specific attributes here (e.g., role, is_active)
        ];
    }
}
