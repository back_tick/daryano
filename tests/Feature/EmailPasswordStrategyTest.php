<?php

namespace Tests\Feature;

use App\Models\Admin;
use App\Strategies\EmailPasswordStrategy;
use Dotenv\Validator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Tests\TestCase;

class EmailPasswordStrategyTest extends TestCase
{
    use RefreshDatabase;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_authenticate_with_valid_credentials_succeeds()
    {
        $user = Admin::factory()->create(['password' => 'secret']);

        $credentials = [
            'email' => $user->email,
            'password' => 'secret',
        ];

        $strategy = new EmailPasswordStrategy();
        $this->assertTrue($strategy->authenticate($credentials));
        $this->assertAuthenticated('admin');
    }
    public function test_authenticate_with_invalid_credentials_fails()
    {
        $user = Admin::factory()->create(['password' => 'secret']);

        $credentials = [
            'email' => $user->email,
            'password' => 'wrong',
        ];

        $strategy = new EmailPasswordStrategy();
        $this->assertFalse($strategy->authenticate($credentials));
        $this->assertGuest();
    }
    public function test_validation_rules_are_correct()
    {
        $strategy = new EmailPasswordStrategy();
        $rules = $strategy->validationRules();

        $this->assertEquals([
            'email' => 'required|email',
            'password' => 'required|min:8', // Adjust minimum length as needed
        ], $rules);
    }


}
