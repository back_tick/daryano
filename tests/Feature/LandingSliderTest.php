<?php

namespace Tests\Feature;

use App\Models\LandingSlider;
use App\Repositories\LandingSliderRepository;
use App\Services\LandingSliderService;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Nette\Schema\ValidationException;
use Tests\TestCase;

class LandingSliderTest extends TestCase
{
    private $repository;
    private $service;

    public function setUp(): void
    {
        parent::setUp();
        $this->repository = new LandingSliderRepository(new Storage()); // Assuming new LandingSlider creates an instance
        $this->service = new LandingSliderService($this->repository);
    }

    public function tearDown() : void
    {
        parent::tearDown();
        // Clean up test data or reset database state
    }
    public function testCreateSuccess()
    {
        $this->withoutExceptionHandling();
        $data = [
            'file_path' => 'path/to/file.jpg',
            'available' => 'show',
            'type' => 'investor',
            'title' => 'Test Slider',
            'description' => 'This is a test slider.',
        ];

        $slider = $this->service->create($data);

        $this->assertInstanceOf(LandingSlider::class, $slider); // Verify created object type
        $this->assertEquals($data['file_path'], $slider->file_path); // Check specific attribute
        // Additional assertions based on your model and validation

        // Clean up the created slider from the database
    }
    public function testGetAll()
    {
        // Create some test sliders in the database beforehand

        $sliders = $this->service->getAll();

        $this->assertCount(1, $sliders); // Assuming you created 2 test sliders
        $this->assertInstanceOf(Collection::class, $sliders); // Verify collection type
        // More assertions based on your model data

        // Clean up test data if needed
    }
    public function testGetById()
    {
        // Create a test slider in the database with ID 1

        $slider = $this->service->getById(1);

        $this->assertInstanceOf(LandingSlider::class, $slider);
        $this->assertEquals(1, $slider->id); // Verify specific attribute
        // More assertions based on your model data
    }
    public function testUpdateSuccess()
    {
        // Create a test slider in the database with ID 1

        $data = [
            'title' => 'Updated Title',
        ];

        $slider = $this->service->update(1, $data);

        $this->assertInstanceOf(LandingSlider::class, $slider);
        $this->assertEquals('Updated Title', $slider->title); // Verify updated attribute
        // More assertions based on your model data and validation

        // Clean up test data if needed
    }
}
