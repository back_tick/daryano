<?php


namespace App\Services;


use App\Repositories\LandingSliderRepositoryInterface;

class LandingSliderService implements LandingSliderServiceInterface
{

    private $repository;

    public function __construct(LandingSliderRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function getAll()
    {
        return $this->repository->all();
    }

    public function getById(int $id)
    {
        return $this->repository->find($id);
    }

    public function create(array $data)
    {
        // Perform any additional validation or business logic before creating
        return $this->repository->create($data);
    }

    public function update(int $id, array $data)
    {
        // Perform any additional validation or business logic before updating
        return $this->repository->update($id, $data);
    }

    public function delete(int $id)
    {
        // Perform any additional checks or logic before deleting
        $this->repository->delete($id);
    }
}
