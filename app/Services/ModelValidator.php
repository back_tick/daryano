<?php


namespace App\Services;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class ModelValidator
{
    public function validate(Model $model, array $rules)
    {
        $validator = Validator::make($model->toArray(), $rules);

        if ($validator->fails()) {
            return $validator->errors()->toArray();
        }

        return [];
    }
}
