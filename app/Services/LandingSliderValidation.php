<?php


namespace App\Services;


use App\Models\LandingSlider;
use Illuminate\Database\Eloquent\Model;

class LandingSliderValidation extends ModelValidator
{
    public function rules()
    {
        return [

            'type' => 'required|in:investor,adviser',
            'title' => 'required|string|max:255',
            'description' => 'required|string|max:1000',
        ];
    }

    public function validate(Model $model, array $rules)
    {



        return parent::validate($model, $rules);
    }
}
