<?php

namespace App\Http\Controllers\Auth;

use App\Contracts\AuthenticationStrategy;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    // Determine the authentication strategy based on user input or configuration
    public function login(Request $request)
    {
        $strategy = app(AuthenticationStrategy::class);// Inject dependency
        // Validate user input based on chosen strategy
        $rules = $strategy->validationRules();
        $this->validate($request,$rules);

        // Attempt authentication using the chosen strategy
        if($strategy->authenticate($request->only('email','password'))){
            // Authentication successful, redirect to intended route
         return redirect()->intended('/admin/dashboard');
        }
        // Authentication failed, return with error message
        return back()->withErrors(['Invalid credentials']);
    }

    public function getLogin()
    {
        return view('admin.login');
    }
}
