<?php

namespace App\Http\Controllers;

use App\Models\LandingSlider;
use App\Services\LandingSliderServiceInterface;
use App\Services\LandingSliderValidation;
use Illuminate\Http\Request;
use function Illuminate\Routing\Controllers\only;

class LandingSliderController extends Controller
{
    private $service;
    private $validation;
    public function __construct(LandingSliderServiceInterface $service,LandingSliderValidation $validation)
    {
        $this->service = $service;
        $this->validation = $validation;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


       // LandingSlider::factory()->count(3)->create();


        $sliders = $this->service->getAll();

        $sliders = $this->paginate($sliders, 20, null, ['path' => url('admin/landing_page_index/')]);

        return view('admin.landing_page.index', compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view('admin.landing_page.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->only(['file','title','description','type','available']);
        $request->validate(['file'=>'required|max:2000|mimes:jpg,jpeg,png,pdf']);
        $errors = $this->validation->validate(new LandingSlider($data),$this->validation->rules());




        if (!empty($errors) ) {
            return back()->withErrors($errors);
        }

        $this->service->create($data);

        return redirect()->route('admin.landing_slider.index')->with('success', 'اطلاعات اضافه گردید');


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\LandingSlider  $landingSlider
     * @return \Illuminate\Http\Response
     */
    public function show(LandingSlider $landingSlider)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LandingSlider  $landingSlider
     * @return \Illuminate\Http\Response
     */
    public function edit(LandingSlider $landingSlider)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\LandingSlider  $landingSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LandingSlider $landingSlider)
    {
        $data = $request->only(['title','description','type','available','file']);
        $errors = $this->validation->validate(new LandingSlider($data));

        if (!empty($errors)) {
            return back()->withErrors($errors);
        }

       $this->service->update($landingSlider->id,$data);

        return redirect()->route('admin.landing_page.index')->with('success', 'محتوا با موفقیت بروز رسانی  گردید');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LandingSlider  $landingSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy(LandingSlider $landingSlider)
    {
        $this->service->delete($landingSlider->id);
        return  redirect(route('admin.landing_slider.index'))->with('success'.'محتوا با موفقیت حذف گردید');
    }

    public function showLandingPage()
    {
        $sliders = $this->service->getAll();
        return view('landing_page',compact('sliders'));

    }
}
