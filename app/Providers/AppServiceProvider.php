<?php

namespace App\Providers;

use App\Contracts\AuthenticationStrategy;
use App\Repositories\LandingSliderRepository;
use App\Repositories\LandingSliderRepositoryInterface;
use App\Services\LandingSliderService;
use App\Services\LandingSliderServiceInterface;
use App\Strategies\EmailPasswordStrategy;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $this->app->bind(AuthenticationStrategy::class, EmailPasswordStrategy::class);
        $this->app->bind(LandingSliderRepositoryInterface::class, LandingSliderRepository::class);
        $this->app->bind(LandingSliderServiceInterface::class, LandingSliderService::class);
    }
}
