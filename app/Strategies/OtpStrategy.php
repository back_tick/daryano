<?php


namespace App\Strategies;


class OtpStrategy implements \App\Contracts\AuthenticationStrategy
{

    /**
     * @inheritDoc
     */
    public function authenticate(array $credentials)
    {

    }
    public function validationRules()
    {
        return [
            'otp' => 'required|numeric|min:5|max:5',

        ];
    }
}
