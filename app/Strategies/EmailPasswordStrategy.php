<?php


namespace App\Strategies;


use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class EmailPasswordStrategy implements \App\Contracts\AuthenticationStrategy
{

    /**
     * @inheritDoc
     */
    public function authenticate(array $credentials)
    {
        // Use Laravel's Auth::attempt for email/password authentication

        $admin = Admin::where('email', $credentials['email'])->first();


        if (!$admin)
            return false;
        if ($credentials['password']=== $admin->password) {
            // Authentication successful, store user information in session

            auth()->guard('admin')->loginUsingId($admin->id);
            return true;
        }

        return false;
    }

    public function validationRules()
    {
        return [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ];
    }
}
