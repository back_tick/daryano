<?php


namespace App\Repositories;


use App\Models\LandingSlider;
use Illuminate\Support\Facades\Storage;
use League\Flysystem\Visibility;

class LandingSliderRepository implements LandingSliderRepositoryInterface
{
    private $storage;

    public function __construct(Storage $storage)
    {
        $this->storage = $storage;
    }
    public function all()
    {
        return LandingSlider::all();
    }

    public function find(int $id)
    {
        return LandingSlider::findOrFail($id);
    }

    public function create(array $data)
    {


        if (isset($data['file'])) {

            $path = Storage::disk('public')->putFile('uploads', $data['file'], Visibility::PUBLIC);
            $data['file_path'] = $path;
        }

        return LandingSlider::create($data);
    }

    public function update(int $id, array $data)
    {
        $slider = $this->find($id);
        $slider->update($data);
        return $slider;
    }

    public function delete(int $id)
    {
        $this->find($id)->delete();
    }
}
