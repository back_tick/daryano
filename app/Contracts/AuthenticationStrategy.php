<?php


namespace App\Contracts;


interface AuthenticationStrategy
{
    /**
     * Attempt to authenticate the user with the given credentials.
     *
     * @param array $credentials An associative array containing credentials (e.g., email, password)
     * @return bool True if authentication is successful, false otherwise
     */
    public function authenticate(array $credentials);

    public function validationRules();
}
