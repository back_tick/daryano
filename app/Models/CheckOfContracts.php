<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CheckOfContracts extends Model
{
    use HasFactory;
    public function contract()
    {
        return $this->belongsTo(Contracts::class);
    }
}
