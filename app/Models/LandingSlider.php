<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LandingSlider extends Model
{
    use HasFactory;
    protected $fillable = [
        // ... other fillable attributes
        'file_path',
        'title',
        'description',
        'type',
        'available',
    ];
}
