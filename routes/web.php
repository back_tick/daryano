<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\LandingSliderController;
use App\Http\Controllers\Auth\LoginController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [LandingSliderController::class, 'showLandingPage'])->name('landing_page');
Route::get('/showFile/{model}', [\App\Http\Controllers\FileController::class, 'showFile'])->name('landing_page');

Route::get('/admin/login', [LoginController::class, 'getLogin'])->name('admin.login');
Route::post('/admin/verify', [LoginController::class, 'login'])->name('admin.verify.login');


Route::group(['prefix' => 'admin', 'middleware' => 'auth.admin'], function () {
    Route::get('/dashboard', [AdminController::class, 'adminDashboard'])->name('admin.dashboard');
    Route::get('/landing_page_setting', [LandingSliderController::class, 'create'])->name('lading_page.setting.create');
    Route::post('/landing_page_store', [LandingSliderController::class, 'store'])->name('admin.landing_slider.store');
    Route::get('/landing_page_index', [LandingSliderController::class, 'index'])->name('admin.landing_slider.index');
    Route::get('/landing_page_delete/{landingSlider}', [LandingSliderController::class, 'destroy'])->name('admin.landing_slider.delete');


});
